<?php
/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Eliademy_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection))
        {
            $pageId = Mage::app()->getRequest()->getParam('page_id', Mage::app()->getRequest()->getParam('id', false));
            $page = Mage::getModel('cms/page')->load($pageId);
            $pageContent = $page->getContent();

            if (isset($_GET["sku"]))
            {
                $this->_productCollection = Mage::getModel('catalog/product')->getCollection();

                $this->_productCollection->addFieldToFilter('attribute_set_id',
                    Mage::getModel('eav/entity_attribute_set')->load("COURSE", 'attribute_set_name')->getAttributeSetId());

                $this->_productCollection->getSelect()->limit(1);
                $this->_productCollection->addAttributeToSelect("name");
                $this->_productCollection->addAttributeToSelect("hidden_from_public");

                $this->_productCollection->addAttributeToFilter('hidden_from_public',
                                 array(array('eq' => ''), array('null' => true), array('eq' => 'No')), 'left');

                $this->_productCollection->addFieldToFilter('visibility', array(array('eq' => 2), array('eq' => 3), array('eq' => 4)));
                $this->_productCollection->addFieldToFilter('sku', array('eq' => $_GET["sku"]));
            }
            else if (in_array($pageContent, array("top_free", "top_paid")))
            {
                // "Special" page - get all items.
                $this->_productCollection = Mage::getModel('catalog/product')->getCollection();

                $this->_productCollection->addFieldToFilter('attribute_set_id',
                    Mage::getModel('eav/entity_attribute_set')->load("COURSE", 'attribute_set_name')->getAttributeSetId());

                $this->_productCollection->getSelect()->limit(24);
                $this->_productCollection->addAttributeToSelect("name");
                $this->_productCollection->addAttributeToSelect("hidden_from_public");

                $this->_productCollection->addAttributeToFilter('hidden_from_public',
                                 array(array('eq' => ''), array('null' => true), array('eq' => 'No')), 'left');

                $this->_productCollection->addFieldToFilter('visibility', array(array('eq' => 2), array('eq' => 3), array('eq' => 4)));

                switch ($pageContent)
                {
                    case "top_free":
                        $this->_productCollection->addFieldToFilter('price', array('eq' => 0));
                        $this->_productCollection->addFieldToFilter('participants', array('gt' => 5));
                        break;

                    case "top_paid":
                        $this->_productCollection->addFieldToFilter('price', array('gt' => 0));
                        $this->_productCollection->addFieldToFilter('participants', array('gt' => 0));
                        break;
                }
            }
            else
            {
                // "Normal" page - normal procedure.
                $layer = $this->getLayer();
                /* @var $layer Mage_Catalog_Model_Layer */
                if ($this->getShowRootCategory()) {
                    $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
                }

                // if this is a product view page
                if (Mage::registry('product')) {
                    // get collection of categories this product is associated with
                    $categories = Mage::registry('product')->getCategoryCollection()
                        ->setPage(1, 1)
                        ->load();
                    // if the product is associated with any category
                    if ($categories->count()) {
                        // show products from this category
                        $this->setCategoryId(current($categories->getIterator()));
                    }
                }

                $origCategory = null;
                if ($this->getCategoryId()) {
                    Mage::log($this); 
                    $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                    if ($category->getId()) {
                        $origCategory = $layer->getCurrentCategory();
                        $layer->setCurrentCategory($category);
                        $this->addModelTags($category);
                    }
                }

                $this->_productCollection = $layer->getProductCollection();

                $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());
            }

            if (!@$_GET["q"]) {
                $this->_productCollection->setOrder('participants', 'desc');
                $this->_productCollection->getSelect()->reset(Zend_Db_Select::ORDER);
                $this->_productCollection->getSelect()->order('CAST(`participants` AS SIGNED) DESC');
            } else {
                $this->_productCollection->setOrder('relevance', 'desc');
                $this->_productCollection->getSelect()->reset(Zend_Db_Select::ORDER);
                $this->_productCollection->getSelect()->order('CAST(`relevance` AS SIGNED) DESC');
            }

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // set collection to toolbar and apply sort
        $toolbar->setCollection($this->_getProductCollection());

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml()
    {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection)
    {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code)
    {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate()
    {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }

    /**
     * Retrieve block cache tags based on product collection
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getProductCollection())
        );
    }
}
