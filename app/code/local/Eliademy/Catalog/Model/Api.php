<?php

class Eliademy_Catalog_Model_Api extends Mage_Api_Model_Resource_Abstract
{
    function info($course, $stars, $text, $username, $userid)
    {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $course);

        $review = Mage::getModel('review/review');
        $review->setEntityPkValue($product->getId());
        $review->setTitle($userid);
        $review->setDetail($text);
        $review->setEntityId(1);
        $review->setStatusId(empty($text) ? 1 : 2);        // 1 - approved, 2 - pending
        $review->setNickname($username);
        $review->setReviewId($review->getId());
        $review->setStores($product->getStoreIds());
        $review->save();
        $review->aggregate();

        if ($stars > 0)
        {
            $rating = Mage::getModel('rating/rating');
            $rating->setRatingId(1);
            $rating->setReviewId($review->getId());
            $rating->addOptionVote($stars, $product->getId());
        }
    }
}
