<?php

require_once __DIR__ . "/../../config.php";

class Eliademy_Catalog_Model_Observer
{
    public function __construct()
    { }

    public function register_payment($observer)
    {
        $payment = $observer->getEvent()->getPayment();
        $order = $payment->getOrder();

        // Non-free courses/licences - register payments in moodle
        foreach ($order->getAllItems() as $item)
        {
            $attributeSetName = Mage::getModel("eav/entity_attribute_set")->load(Mage::getModel('catalog/product')->load($item->getProductId())->getAttributeSetId())->getAttributeSetName();

            if ($attributeSetName == "LICENCE")
            {
                $url = MOODLE_ROOT . "theme/monorail/ext/ajax_register_licence_payment.php?user=" . $order->getCustomerLastname() .
                    "&count=" . $item->getQtyOrdered() . "&details=" . urlencode(json_encode($payment->getData()));

                mail("support@cloudberrytec.com", "Payment for licence registered.",
                    "Someone has paid for E4B licences. Order id: " . $order->getId(). ".");
            }
            else if ($attributeSetName == "SUPERLICENCE")
            {
                $url = MOODLE_ROOT . "theme/monorail/ext/ajax_register_licence_payment.php?user=" . $order->getCustomerLastname() .
                    "&count=-1&details=" . urlencode(json_encode($payment->getData()));

                mail("support@cloudberrytec.com", "Payment for unlimited licences registered.",
                    "Someone has paid for unlimited E4B licences. Order id: " . $order->getId(). ".");
            }
            else
            {
                $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());

                $url = MOODLE_ROOT . "theme/monorail/ext/ajax_register_course_payment.php" .
                    "?course=" . $item->getSku() .
                    "&user=" . $order->getCustomerLastname() .
                    "&totaltax=" . ((float) $quote->getGrandTotal() - (float) $quote->getSubtotal()) .
                    "&totalprice=" . ((float) $quote->getGrandTotal());

                mail("sotiris.makrygiannis@cloudberrytec.com", "Payment for course received.",
                    "Someone has just paid for a course. Order id in magento: " . $order->getId(). ".");
            }

            if (trim(file_get_contents($url)) != "OK")
            {
                error_log("FAILED to register payment in moodle. Notifying support.");

                mail("support@cloudberrytec.com", "Payment for course or licence order failed.",
                    "Unable to register payment for course or licence. Order id: " . $order->getId(). ". Please check manually.");
            }
        }
    }

    public function register_order($observer)
    {
        $oid = $observer->getEvent()->getOrder()->getId();
        $order = Mage::getModel('sales/order')->load($oid);
        $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());
        $totals = $quote->getTotals();

        // Free - enroll/apply immediately
        if ($totals["grand_total"]->getValue() == 0)
        {
            foreach ($order->getAllItems() as $item)
            {
                $attributeSetName = Mage::getModel("eav/entity_attribute_set")->load(Mage::getModel('catalog/product')->load($item->getProductId())->getAttributeSetId())->getAttributeSetName();

                if ($attributeSetName == "LICENCE")
                {
                    $url = MOODLE_ROOT . "theme/monorail/ext/ajax_register_licence_payment.php?user=" . $order->getCustomerLastname() .
                        "&count=" . $item->getQtyOrdered() . "&details=" . urlencode("FREE");

                    mail("support@cloudberrytec.com", "FREE licence registered.",
                        "Someone got E4B licences for FREE! Order id: " . $order->getId(). ".");
                }
                else if ($attributeSetName == "SUPERLICENCE")
                {
                    $url = MOODLE_ROOT . "theme/monorail/ext/ajax_register_licence_payment.php?user=" . $order->getCustomerLastname() .
                        "&count=-1&details=" . urlencode("FREE");

                    mail("support@cloudberrytec.com", "FREE unlimited licences registered.",
                        "Someone got unlimited E4B licences for FREE! Order id: " . $order->getId(). ".");
                }
                else
                {
                    $url = MOODLE_ROOT . "theme/monorail/ext/ajax_user_control.php?course=" . $item->getSku() . "&user=" . $order->getCustomerLastname();
                }

                file_get_contents($url);
            }
        }
    }

    public function register_rating($observer)
    {
        $data1 = $observer->getEvent()->getData();
        $data2 = $data1["data_object"]->getData();

        $rating = Mage::getModel('review/review_summary')->load($data2["entity_pk_value"]);

        if ($rating["rating_summary"])
        {
            // Send rating to moodle

            $product = Mage::getModel('catalog/product')->load($data2["entity_pk_value"]);

            $stars = (int) ($rating["rating_summary"] / 20);

            $url = MOODLE_ROOT . "theme/monorail/ext/ajax_course_rating.php?course=" . $product->getSku() . "&rating=" . $stars;

            file_get_contents($url);
        }
    }
}
