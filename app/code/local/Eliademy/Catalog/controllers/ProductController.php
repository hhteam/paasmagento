<?php
require_once 'Mage/Catalog/controllers/ProductController.php';

class Eliademy_Catalog_ProductController extends Mage_Catalog_ProductController
{
    public function viewAction()
    {
        // Get initial data from request
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId  = (int) $this->getRequest()->getParam('id', false);
        $productSku  = $this->getRequest()->getParam('sku', false);
        $specifyOptions = $this->getRequest()->getParam('options');

        if ($productSku && !$productId)
        {
            $cProd = Mage::getModel('catalog/product');
            $productId = $cProd->getIdBySku("$productSku");
        }

        // Prepare helper and params
        $viewHelper = Mage::helper('catalog/product_view');

        $params = new Varien_Object();
        $params->setCategoryId($categoryId);
        $params->setSpecifyOptions($specifyOptions);

        // Render page
        try {
            $viewHelper->prepareAndRender($productId, $this, $params);
        } catch (Exception $e) {
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $this->_forward('noRoute');
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }
    }
}
