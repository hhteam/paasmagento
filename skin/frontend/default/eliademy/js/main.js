(function ($)
{
    var updatePage = function ()
    {
        if (_Header.info instanceof Object && parseInt(_Header.info.userid))
        {
            $(".add-to-cart").show();
            $(".go-to-course").hide();
            $(".user-display").show();
            $(".hide").hide();

            if (parseInt(_Header.info.cohort))
            {
                $(".cohort-display").show();
            }
            else
            {
                $(".cohort-display").hide();
            }

            if (typeof _Header.info.courses == "object")
            {
                for (var i in _Header.info.courses)
                {
                    $(".add-to-cart-" + i).hide();
                    $(".go-to-course-" + i).show();

                    if (!_Header.info.courses[i].canedit)
                    {
                        $(".user-display-" + i).show();
                    }
                    else
                    {
                        $(".user-display-" + i).hide();
                    }
                }
            }

            if (typeof CourseCode == "string")
            {
                if (_Header.info.paidCourses instanceof Object && _Header.info.paidCourses[CourseCode])
                {
                    $("#confirm-purchase-" + CourseCode).show();
                    $("#payment-method-selection").hide();
                    $(".non-payment-item").hide();
                    $(".payment-item").show();

                    $(".addressInput").removeAttr("disabled");
                }
                else if (window.location.hash == "#payment" && !(CourseCode in _Header.info.courses))
                {
                    $(".non-payment-item").hide();
                    $(".payment-item").show();
                }
            }

            if (typeof _Header.info.address == "object" && _Header.info.address && _Header.info.address.name)
            {
                $("#inputName").val(_Header.info.address.name);
                $("#inputAddress").val(_Header.info.address.address);
                $("#inputCity").val(_Header.info.address.city);
                $("#inputState").val(_Header.info.address.state);
                $("#inputCountry").val(_Header.info.address.country);
                $("#inputZip").val(_Header.info.address.zip);
            }
            else
            {
                $(".addressInput").val("");
            }

            if (typeof LicencePage != "undefined" && LicencePage)
            {
                if (_Header.info.org)
                {
                    $("#org_name_field").val(_Header.info.org);
                }
                else
                {
                    window.location.href = E.home + "e4b-error";
                }
            }
        }
        else
        {
            if (window.location.hash == "#payment") {
                $.cookie("loginRedirect", window.location.href, { path: "/" })
                window.location.href = E.home + "/login";
            }

            $(".cohort-display").hide();
            $(".user-display").hide();
            $(".hide").hide();

            $(".add-to-cart").show();
            $(".go-to-course").hide();

            if (typeof LicencePage != "undefined" && LicencePage)
            {
                $("#org_name_field").val("");
            }
        }
    };

    $(function ()
    {
        $("#summary_field").change(function ()
        {
            if ($(this).val())
            {
                $("#review-title").text($(this).val());
            }
        });

        $(".show-payment-items").click(function ()
        {
            if (!(_Header.info instanceof Object) || !parseInt(_Header.info.userid))
            {
                $.cookie("loginRedirect", window.location.href + "#payment", { path: "/" })
                window.location.href = E.home + "/login";
            }
            else
            {
                $(".non-payment-item").hide();
                $(".payment-item").show();
                window.location.hash = "#payment";
            }
        });

        if ($(window).height() < $(".fixed-sidebar").height() + 250)
        {
            $(".fixed-sidebar").css({ position: "static" });
        }

        if (typeof checkoutProgress != "undefined")
        {
            var opts = { lines: 13, length: 7, width: 4, radius: 10, corners: 1, rotate: 0, color: '#000', speed: 1,
                      trail: 60, shadow: false, hwaccel: true, className: 'spinner', zIndex: 2e9, top: 'auto' };

            var el = $("<div>")
                .attr({"class": "processing_message"})
                .html(checkoutMessage)
                .css({ "text-align": "center", position: "absolute", top: $(window).height() / 2 - 170, left: 0, right: 0 })
                .appendTo(document.body);

            el.append($((new Spinner(opts)).spin().el).css({ left: $(window).width() / 2 - 10, top: 30 }));
        }

        if (typeof CouponStatus != "undefined")
        {
            if (CouponStatus == 2)
            {
                $("#apply-coupon-btn").after("&nbsp;<span class=\"help-inline\">This coupon is already redeemed</span>");
                $("#apply-coupon-controls").addClass("error");
            }
        }

        $("#apply-coupon-btn").click(function (ev)
        {
            ev.preventDefault();

            window.location.hash = "#payment";
            window.location.search = "?coupon=" + $("#inputCoupon").val();
        });

        $("#payment-country-selector").change(function ()
        {
            document.cookie = "user_country=" + $("#payment-country-selector").val() + "; path=/; expires=" + (new Date((new Date()).getTime() + 172800000)).toUTCString();
            location.reload(false);
        });

        $("li[data-topic] a").click(function (ev)
        {
            ev.preventDefault();

            $("li[data-topic]").removeClass("active");

            var topic = parseInt($(ev.target).closest("[data-topic]").addClass("active").attr("data-topic"));

            if (topic)
            {
                if (_Header.info instanceof Object && parseInt(_Header.info.userid) && (CourseCode in _Header.info.courses))
                {
                    window.location.href = E.ui + "courses/" + CourseCode + "/" + topic;
                }
                else
                {
                    $("[data-content='overview']").hide();
                    $("[data-content='topic']").show();
                }
            }
            else
            {
                $("[data-content='overview']").show();
                $("[data-content='topic']").hide();
            }
        });

        if (typeof LicencePage != "undefined" && LicencePage)
        {
            var updateLicenceNum = function (num)
            {
                if (isNaN(num) || num < 10)
                {
                    num = 10;
                }
                else if (num > 1000)
                {
                    num = 1000;
                }

                $.cookie("licence_count", num, { path: "/" });

                if (!$("#inputCoupon").val())
                {
                    var newTotal = String(Math.round((CurrentTotalPrice / CurrentQuantity) * num * 100));

                    $("#licence_total_price").text("€" + newTotal.substr(0, newTotal.length - 2) + "." + newTotal.substr(newTotal.length - 2));
                    $("#licence_user_count").val(num);
                    $("#product_quantity_field").val(num);
                }
                else
                {
                    document.location.reload();
                }
            };

            $("#licence_add_user_btn").click(function ()
            {
                if ($("#inputCoupon").val())
                {
                    $("#licence_add_user_btn").attr("disabled", "disabled");
                }

                updateLicenceNum(parseInt($("#licence_user_count").val()) + 1);
            });

            $("#licence_del_user_btn").click(function ()
            {
                if ($("#inputCoupon").val())
                {
                    $("#licence_del_user_btn").attr("disabled", "disabled");
                }

                updateLicenceNum(parseInt($("#licence_user_count").val()) - 1);
            });

            $("#licence_user_count").change(function()
            {
                updateLicenceNum(parseInt($("#licence_user_count").val()));
            });
        }

        updatePage();

        _Header.init($("#header-items").get(0), "catalog");

        $('[data-magic-highlight="true"][href="' + document.URL + '"]').addClass("active");

        FastClick.attach(document.body);

        for (var i=0; i<E.init.length; i++)
        {
            E.init[i]();
        }
    });

}) (window.jQuery);
